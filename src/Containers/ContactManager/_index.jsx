import React, { Component } from "react";
import { Card, Paper, withStyles, Divider } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import Fab from "@material-ui/core/Fab";
import ContactCard from "../../Components/ContactCard";
import grey from "@material-ui/core/colors/grey";
import ContactForm from "./../../Components/ContactForm";
import {
    contactsMachine,
    SHOW_MODAL,
    CLOSE_MODAL,
    modalOpen
} from "./../../xstate/contactsMachine";
import { interpret } from "xstate";

const ICON_DIMENSION = 42;
let dataSet = [
    {
        id: "1212",
        name: "Raghav Chinda",
        email: "raghav17880.s@gmail.com"
    },
    {
        id: "1213",
        name: "Raghav Chinda",
        email: "raghav17880.s@gmail.com"
    },
    {
        id: "1214",
        name: "Raghav Chinda",
        email: "raghav17880.s@gmail.com"
    },
    {
        id: "1215",
        name: "Raghav Chinda",
        email: "raghav17880.s@gmail.com"
    }
];

// export default function ContactManager() {
//     const [state, send] = useMachine(contactsMachine);
//     useHashChange(e => {
//         send(SHOW_MODAL);
//     });
//     const {} = state.context;
//     let { open, selected } = state;
//     return (
//         <Paper
//             style={{
//                 flex: 1,
//                 margin: "21px",
//                 padding: "15px",
//                 backgroundColor: grey["100"],
//                 minHeight: "100vh"
//             }}
//         >
//             <Card
//                 className="mv2"
//                 style={{
//                     display: "flex",
//                     justifyContent: "space-between",
//                     alignItems: "center",
//                     padding: "21px"
//                 }}
//             >
//                 <span className="h2" style={{ color: grey["900"] }}>
//                     Contact Manager
//                 </span>
//                 <Fab
//                     color="primary"
//                     aria-label="Add"
//                     onClick={() => {
//                         send({ type: SHOW_MODAL });
//                     }}
//                     style={{
//                         height: ICON_DIMENSION,
//                         width: ICON_DIMENSION
//                     }}
//                 >
//                     <Icon style={{ fontSize: ICON_DIMENSION / 2 }}>add</Icon>
//                 </Fab>
//             </Card>
//             <Divider />
//             <Paper>
//                 {dataSet.map(contact => (
//                     <ContactCard
//                         editPressHandler={send(SHOW_MODAL)}
//                         key={contact.id}
//                         {...contact}
//                     />
//                 ))}
//             </Paper>
//             <ContactForm
//                 selected={selected}
//                 // name={this.state.name}
//                 // email={this.state.email}
//                 // handleAdd={this.handleAdd}
//                 // handleDelete={this.handleDelete}
//                 // handleUpdate={this.handleUpdate}
//                 // handleCancel={this.handleCancel}
//                 // onChangeHandler={this.onChangeHandler}
//                 open={open}
//             />
//         </Paper>
//     );
// }

export default class ContactManager extends Component {
    state = {
        current: contactsMachine.initialState
    };

    componentDidMount() {
        this.service.start();
    }

    service = interpret(contactsMachine).onTransition(current =>
        this.setState({ current })
    );

    handleCancel = () =>
        this.setState({ open: false, selected: null, name: "", email: "" });
    handleOpen = () => this.setState({ open: true });
    handleDelete = () => {};
    handleUpdate = () => {};
    handleAdd = () => {};
    editPressHandler = id => {
        let selected = dataSet.find(contact => contact.id === id);
        this.setState({
            selected: id,
            open: true,
            name: selected.name,
            email: selected.email
        });
    };
    onChangeHandler = (e, fieldName) => {
        this.setState({ [fieldName]: e.target.value });
    };
    render() {
        let { current } = this.state;
        let { send } = this.service;
        console.log("this.state", current.matches(modalOpen));
        return (
            <Paper
                style={{
                    flex: 1,
                    margin: "21px",
                    padding: "15px",
                    backgroundColor: grey["100"],
                    minHeight: "100vh"
                }}
            >
                <Card
                    className="mv2"
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        padding: "21px"
                    }}
                >
                    <span className="h2" style={{ color: grey["900"] }}>
                        Contact Manager
                    </span>
                    <Fab
                        color="primary"
                        aria-label="Add"
                        onClick={this.handleOpen}
                        style={{
                            height: ICON_DIMENSION,
                            width: ICON_DIMENSION
                        }}
                    >
                        <Icon style={{ fontSize: ICON_DIMENSION / 2 }}>
                            add
                        </Icon>
                    </Fab>
                </Card>
                <Divider />
                <Paper>
                    {dataSet.map(contact => (
                        <ContactCard
                            editPressHandler={this.editPressHandler}
                            key={contact.id}
                            {...contact}
                        />
                    ))}
                </Paper>
                <ContactForm
                    // selected={selected}
                    name={this.state.name}
                    email={this.state.email}
                    handleAdd={this.handleAdd}
                    handleDelete={this.handleDelete}
                    handleUpdate={this.handleUpdate}
                    handleCancel={() => {
                        send(CLOSE_MODAL);
                    }}
                    onChangeHandler={this.onChangeHandler}
                    open={current.matches(modalOpen)}
                />
            </Paper>
        );
    }
}
