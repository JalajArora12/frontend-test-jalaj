import React, { Component } from "react";
import { Card, Paper, withStyles, Divider } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import ApolloClient from "apollo-boost";
import gql from "graphql-tag";
import Fab from "@material-ui/core/Fab";
import ContactCard from "../../Components/ContactCard";
import grey from "@material-ui/core/colors/grey";
import ContactForm from "./../../Components/ContactForm";
import {
    contactsMachine
} from "./../../xstate/contactsMachine";
import { interpret } from "xstate";

const ICON_DIMENSION = 42;

const client = new ApolloClient({
    uri: "http://localhost:3001"
});

export default class ContactManager extends Component {
    constructor(props) {
        super(props);
        let selectedId = props.match.params.id || null;
        this.state = {
            open: false,
            selected: selectedId,
            name: "",
            email: "",
            contacts: []
        };
        this.service = interpret(contactsMachine).onTransition(current =>
            this.setState({ contacts: current.context.contacts })
        );
    }

    async componentDidMount() {
        try {
            this.service.start();
            let contacts = await this.getContacts();
            if (contacts.length > 0) {
                let { selected } = this.state;
                let selectedContact =
                    selected && (await this.getContactById(contacts, selected));
                if (selectedContact) {
                    this.setState({
                        open: true,
                        name: selectedContact.name,
                        email: selectedContact.email
                    });
                } else {
                    this.props.history.replace("");
                }
            }
        } catch (err) { }
    }
    componentWillUnmount() {
        this.service.stop();
    }

    handleCancel = () => {
        this.setState({ open: false, name: "", selected: null, email: "" });
        this.props.history.replace("/");
    };
    handleOpen = () => this.setState({ open: true });
    handleDelete = async () => {
        try {
            let { selected, contacts } = this.state;
            let NEW_CONTACTS = [...contacts];
            let data = await this.deleteContact(selected);
            let index = NEW_CONTACTS.findIndex(
                contact => contact.id === selected
            );

            if (index > -1) {
                NEW_CONTACTS.splice(index, 1);
            }
            this.setState({
                contacts: [...NEW_CONTACTS],
                open: false,
                name: "",
                email: "",
                selected: null
            });
        } catch (err) {
            console.log(err);
        }
    };
    handleUpdate = async () => {
        try {
            let { selected, contacts, name, email } = this.state;
            await this.updateContact({ name, email, id: selected });
            let NEW_CONTACTS = [...contacts];
            let index = NEW_CONTACTS.findIndex(item => item.id === selected);

            NEW_CONTACTS[index] = { name, email, selected };
            this.setState({
                contacts: [...NEW_CONTACTS],
                open: false,
                name: "",
                selected: null,
                email: ""
            });
        } catch (err) {
            console.log(err);
        }
    };
    handleAdd = async () => {
        let { name, email } = this.state;
        try {
            let { data } = await this.postContact(name, email);
            this.setState({
                contacts: [...this.state.contacts, data.addContact],
                open: false,
                name: "",
                selected: null,
                email: ""
            });
        } catch (err) {
            console.log(err);
        }
    };
    editPressHandler = id => {
        let { contacts } = this.state;
        this.props.history.replace(id);
        let selected = this.getContactById(contacts, id);
        this.setState({
            selected: id,
            open: true,
            name: selected.name,
            email: selected.email
        });
    };
    onChangeHandler = (e, fieldName) => {
        this.setState({ [fieldName]: e.target.value });
    };
    getContactById = (contacts, id) => {
        let selected = contacts && contacts.find(contact => contact.id === id);
        if (selected) return selected;
        return new Promise(async (resolve, reject) => {
            try {
                let { data } = await client.query({
                    query: gql`
                         {
                             contact(id:${id}){
                                 id
                                 name
                                 email
                             }
                         }
                     `
                });
                resolve(data.contact);
            } catch (err) {
                reject(err);
            }
        });
    };
    getContacts = () => {
        return new Promise(async (resolve, reject) => {
            try {
                let { data } = await client.query({
                    query: gql`
                        {
                            contacts {
                                id
                                name
                                email
                            }
                        }
                    `
                });
                this.setState({ contacts: [...data.contacts] });
                resolve(data.contacts);
            } catch (err) {
                reject(err);
            }
        });
    };
    postContact = (name, email) => {
        return client.mutate({
            variables: { contact: { name, email } },
            mutation: gql`
                mutation addContact($contact: InputContact!) {
                    addContact(contact: $contact) {
                        id
                        email
                        name
                    }
                }
            `
        });
    };
    deleteContact = id => {
        return client.mutate({
            variables: { id },
            mutation: gql`
                mutation deleteContact($id: ID!) {
                    deleteContact(id: $id)
                }
            `
        });
    };
    updateContact = contact => {
        let { name, email, id } = contact;
        return client.mutate({
            variables: { contact: { id, name, email } },
            mutation: gql`
                mutation updateContact($contact: InputContact!) {
                    updateContact(contact: $contact) {
                        id
                        email
                        name
                    }
                }
            `
        });
    };
    render() {
        let { open, selected, name, email, contacts } = this.state;
        return (
            <Paper
                style={{
                    flex: 1,
                    margin: "21px",
                    padding: "15px",
                    backgroundColor: grey["100"],
                    minHeight: "100vh"
                }}
            >
                <Card
                    className="mv2"
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        padding: "21px"
                    }}
                >
                    <span className="h2" style={{ color: grey["900"] }}>
                        Contact Manager
                    </span>
                    <Fab
                        color="primary"
                        aria-label="Add"
                        onClick={this.handleOpen}
                        style={{
                            height: ICON_DIMENSION,
                            width: ICON_DIMENSION
                        }}
                    >
                        <Icon style={{ fontSize: ICON_DIMENSION / 2 }}>
                            add
                        </Icon>
                    </Fab>
                </Card>
                <Divider />
                <Paper>
                    {contacts.map((contact, i) => (
                        <ContactCard
                            key={i}
                            editPressHandler={this.editPressHandler}
                            {...contact}
                        />
                    ))}
                </Paper>
                <ContactForm
                    selected={selected}
                    name={name}
                    email={email}
                    handleAdd={this.handleAdd}
                    handleDelete={this.handleDelete}
                    handleUpdate={this.handleUpdate}
                    handleCancel={this.handleCancel}
                    onChangeHandler={this.onChangeHandler}
                    open={open}
                    editPressHandler={this.editPressHandler}
                />
            </Paper>
        );
    }
}
