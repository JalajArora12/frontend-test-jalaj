import React, { Component } from "react";
import logo from "./logo.svg";
import ContactManager from "./Containers/ContactManager";
import "./App.css";
import { Switch, Route } from "react-router-dom";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Switch>
                    <Route path="/:id" component={ContactManager} />
                    <Route exact path="/" component={ContactManager} />
                </Switch>
            </div>
        );
    }
}

export default App;
