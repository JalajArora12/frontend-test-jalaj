import { Machine, assign } from "xstate";
import ApolloClient from "apollo-boost";
import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "http://localhost:3001"
});

export const contactsMachine = Machine({
    id: "contacts",
    initial: "list_contacts",
    context: {
        contacts: []
    },
    states: {
        list_contacts: {
            invoke: {
                id: "list",
                src: (ctx, event) =>
                    client.query({
                        query: gql`
                            {
                                contacts {
                                    id
                                    name
                                    email
                                }
                            }
                        `
                    }),
                onDone: {
                    target: "success",
                    actions: assign({
                        contacts: (ctx, event) => event.data.data.contacts
                    })
                },
            }
        },
        success: {}
    },


});
