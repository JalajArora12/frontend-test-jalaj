import React, { Fragment } from "react";
import "./../App.css";
import { Card, Divider, Button } from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import grey from "@material-ui/core/colors/grey";
import Icon from "@material-ui/core/Icon";
const IMAGE_DIMESION = 32;
const ICON_DIMENSION = 38;
let fields = ["email"];

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
String.prototype.getInitials = function() {
    let splitValue = this.split(" ");
    let fn = splitValue[0];
    let ln = splitValue[1];
    return `${fn[0]} ${ln ? ln[0] : ""}`;
};
export default props => {
    let { name, id, editPressHandler } = props;
    return (
        <Fragment>
            <Card className="contactCardContainer">
                <div style={{ display: "flex", alignItems: "center" }}>
                    <Image name={name} />
                    <div style={{ margin: "15px" }}>
                        <div className="h3" style={{ color: grey["A300"] }}>
                            {name}
                        </div>
                        {fields.map((fieldName, i) => (
                            <div
                                key={i}
                                className="mt2"
                                style={{ display: "flex" }}
                            >
                                <span className="f2">
                                    {fieldName.capitalize()}:
                                </span>
                                <span
                                    className="f2 mh2"
                                    style={{ color: grey["600"] }}
                                >
                                    {props[fieldName]}
                                </span>
                            </div>
                        ))}
                    </div>
                </div>
                <Fab
                    color="secondary"
                    aria-label="Add"
                    onClick={() => {
                        editPressHandler(id);
                    }}
                    style={{ height: ICON_DIMENSION, width: ICON_DIMENSION }}
                >
                    <Icon
                        style={{ fontSize: ICON_DIMENSION / 2, color: "white" }}
                    >
                        edit
                    </Icon>
                </Fab>
            </Card>
            <Divider />
        </Fragment>
    );
};

const Image = props => {
    let { name } = props;
    return (
        <Card
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: IMAGE_DIMESION,
                width: IMAGE_DIMESION,
                borderRadius: IMAGE_DIMESION + 3 / 2,
                backgroundColor: `#e${Math.floor(Math.random() * 100000)}`,
                padding: "3px",
                color: "white"
            }}
            className="h3"
        >
            {name.getInitials()}
        </Card>
    );
};
