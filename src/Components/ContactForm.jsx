import React, { Fragment } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import Icon from "@material-ui/core/Icon";
import grey from "@material-ui/core/colors/grey";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

export default props => {
    let {
        open,
        selected,
        handleDelete,
        handleUpdate,
        handleCancel,
        handleAdd,
        name,
        email,
        onChangeHandler
    } = props;
    let disableButtons = !name || !email;
    return (
        <Dialog
            open={open}
            onClose={handleCancel}
            aria-labelledby="form-dialog-title"
            maxWidth={"md"}
            fullWidth
        >
            <DialogContent>
                <div
                    className="mv2"
                    style={{ display: "flex", alignItems: "center" }}
                >
                    <Icon
                        style={{
                            color: grey["600"],
                            fontSize: "28px",
                            marginRight: "9px"
                        }}
                    >
                        contacts
                    </Icon>
                    <TextField
                        onChange={e => {
                            onChangeHandler(e, "name");
                        }}
                        value={name}
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Name"
                        type="name"
                        fullWidth
                    />
                </div>
                <div
                    className="mv2"
                    style={{ display: "flex", alignItems: "center" }}
                >
                    <Icon
                        style={{
                            color: grey["600"],
                            fontSize: "28px",
                            marginRight: "9px"
                        }}
                    >
                        alternate_email
                    </Icon>
                    <TextField
                        onChange={e => {
                            onChangeHandler(e, "email");
                        }}
                        value={email}
                        autoFocus
                        margin="dense"
                        id="email"
                        label="Email Address"
                        type="email"
                        fullWidth
                    />
                </div>
            </DialogContent>
            <DialogActions style={{ margin: 21 }}>
                {selected !== null ? (
                    <Fragment>
                        <Button
                            disabled={disableButtons}
                            variant="contained"
                            onClick={handleDelete}
                            color="secondary"
                        >
                            Delete
                        </Button>
                        <Button
                            disabled={disableButtons}
                            style={{ marginLeft: "8px", marginRight: "8px" }}
                            variant="contained"
                            onClick={handleUpdate}
                            color="primary"
                        >
                            Update
                        </Button>
                    </Fragment>
                ) : (
                    <Button
                        disabled={disableButtons}
                        style={{ marginLeft: "8px", marginRight: "8px" }}
                        variant="contained"
                        color="primary"
                        onClick={handleAdd}
                        color="primary"
                    >
                        Add
                    </Button>
                )}
                <Button variant="contained" onClick={handleCancel}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    );
};
